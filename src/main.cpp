#include <Arduino.h>
#include <RFM69.h>

#define NODEID        1    // keep UNIQUE for each node on same network
#define NETWORKID     100  // keep IDENTICAL on all nodes that talk to each other
#define GATEWAYID     2    // "central" node
#define FREQUENCY     RF69_868MHZ
#define ENCRYPTKEY    "sampleEncryptKey"


RFM69 radio;
String mac;
uint8_t buff[4];
uint8_t test[4] = {0x11,0x12,0x13,0x3c};

void to_hex();

void setup() {
  Serial.begin(115200);

  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.setHighPower();
  radio.encrypt(ENCRYPTKEY);

  radio.sendWithRetry(GATEWAYID,test,4,1,200);

  Serial.println("Init Done");
}

void loop() 
{
  mac = "";
  Serial.print("Write MAC in XX:XX:XX:XX format with newline: ");
  while (!Serial.available())
  {
    ;
  }
  mac = Serial.readStringUntil('\n');
  mac.toUpperCase();
  Serial.print(mac);
  if(mac.length() == 11)
  {
    to_hex();
    if(radio.sendWithRetry(GATEWAYID, buff, 4, 1, 200))
      Serial.println(" ok!");
    else Serial.println(" Done...");
  }
  else
  {
    Serial.println("Wrong input! Try again");
  }
}

void to_hex()
{
  uint8_t temp_higher, temp_lower, pointer = 0;
  mac.remove(2,1);
  mac.remove(4,1);
  mac.remove(6,1);

  for(int i = 0; i < 8; i ++)
  {
    
    if(mac[i] > 47 & mac[i] < 58 & (i%2) == 0) temp_higher = mac[i] - 48;
    else if(mac[i] > 47 & mac[i] < 58 & (i%2) == 1) temp_lower = mac[i] - 48;
    else if(mac[i] > 64 & mac[i] < 91 & (i%2) == 0) temp_higher = mac[i] - 55;
    else if(mac[i] > 64 & mac[i] < 91 & (i%2) == 1) temp_lower = mac[i] - 55;

    if(i % 2 == 1)
    {
      buff[pointer] = (temp_higher << 4) | temp_lower;
      pointer += 1;
    }
  }
}